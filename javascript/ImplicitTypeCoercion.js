// String and number
var mStr1 = "Hello";
var mNum1 = 5;

var mSum1 = mStr1 + mNum1;
console.log(mSum1); // -> Hello5
console.log(typeof(mSum1)); // returns string: the number value has converted to string automatically

// Number and string
console.log(50 + 'hola'); // -> 50hola
console.log(typeof(50 + 'hola')); // number is converted into string

// String and boolean
console.log('hola' + true); // -> holatrue
console.log(typeof('hola' + true)); // boolean is converted into string

// Integer
console.log(typeof("2")); // string
console.log(typeof(+"2")); // number (when using operators on strings they will converted into number)

// Comparison integer and string
var mValue = 2 >= '2';
console.log(typeof(mValue)); // boolean

// Boolean
if (true) {
    console.log('True');
} else {
    console.log('False');
}

if (35435) { // all values are true, exept 0, null, undefined, '' (empty)
    console.log('True');
} else {
    console.log('False');
}

if ("Apple") { // this is also true
    console.log('True');
} else {
    console.log('False');
}

if (0) { // false
    console.log('True');
} else {
    console.log('False');
}

if (null) { // this is also false
    console.log('True');
} else {
    console.log('False');
}

if ("") { // empty is also treated as false 
    console.log('True');
} else {
    console.log('False');
}

// PRACTICE PROBLEMS

console.log("PRATICE PROBLEMS")

// 1. What will be printed in the console?
console.log(true + false);
console.log(12 / "6");
console.log("number" + 15 + 3);
console.log(15 + 3 + "number");
console.log(1 > null);
console.log("foo" + + "bar");
console.log('true' == true);
console.log(false == 'false');
console.log(null == '');
console.log(!!"false" == !!"true");