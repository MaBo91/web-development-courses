var mStr = "This is my test string to practise the JavaScript string function concepts. This is gonna be fun."

// 1. Find the length of the first sentence in the string
console.log(mStr.indexOf("."));

// 2. Find the length of the second sentence in the string
console.log(mStr.length - mStr.indexOf("."));

// 3. Find the first and last occurrence of the word "this"
console.log(mStr.indexOf("This"));
console.log(mStr.lastIndexOf("This"));

// 4. Find the sub-string with length 12 from the START of the string mStr
console.log(mStr.substr(0, 12));

// 5. Find the sub-string with length 12 from the END of the string mStr
console.log(mStr.substr(mStr.length -12, 12));
