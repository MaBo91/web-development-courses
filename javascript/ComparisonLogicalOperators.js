// Comparison-Operators: ==, ===, !=, !==, <, >, <=, >=

if (5 == '5') { // equal to: returns true, because only the variable is compared, not the type
    console.log(true);
}

if (5 === '5') { // triple = compares type and variable
    console.log(true);
} else {
    console.log(false);
}

if (5 != '5') { // not equal to: != (only value of variable is compared)
    console.log(true);
}  else {
    console.log(false);
}

if (5 !== '5') { // not equal equal to: !== (value and type comparison!) 
    console.log(true);
}  else {
    console.log(false);
}

if (5 < 10) { // less than
    console.log(true);
}  else {
    console.log(false);
}

if (15 <= 15) { // equal to or less than
    console.log(true);
}  else {
    console.log(false);
}

if (5 > 10) { // greater than
    console.log(true);
}  else {
    console.log(false);
}

if (15 >= 15) { // equal to or greater than
    console.log(true);
}  else {
    console.log(false);
}

// Logical-Operators: &&, ||, !

if (5 < 10 && 10 < 15) { // and-Operator &&: When both of the conditions are true, it returns true, otherwise false
     console.log(true); 
}   else { 
     console.log(false);
}

if (5 < 10 || 10 < 15) { // or-Operator ||: It returns true, when one of the condition is true, otherwise false
    console.log(true); 
}   else { 
    console.log(false);
}

if (!(5 < 10)) { // not-Operator !: negates the result
    console.log(true); 
}   else { 
    console.log(false);
}

// Ternary operator: expression ? a : b -> when 'expression' is true, 'a' will be returned, if 'expression' is false, 'b' will be returned
2 < 3 ? console.log(true) : console.log(false);


// PRACTICE PROBLEMS
console.log("\n\nPRACTICE PROBLEMS")

// 1. What will be printed in the console?
var a = 10;
var b = 5;
var c = 12;
var e = 8;
var d;

d = parseInt((a * (c - b) / e + (b + c)) <= (e * (c + a) / (b + c) + a));
console.log(d);

if (d == 1) {
    console.log((a * (c - b) / e + (b + c)));
} else {
    console.log((e * (c + a ) / (b + c) + a));
}

// 2. What will be printed in the console?
var n = 2;
var p = 4;
var q = 5;
var w = 3;

if( !((p * q) / n <= (q * w) + n / p )) {
    console.log(++p + w++ + " " + ++n);
} else {
    console.log(--p + q-- + " " + --n);
}

