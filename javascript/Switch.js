var currentDay = "TUE"

if(currentDay == "MON") {
    console.log("Timings: 10:00 - 18:00")
} else if (currentDay == "TUE") {
    console.log("Timings: 09:00 - 17:00")
} else if (currentDay == "WED") {
    console.log("Timings: 09:30 - 17:30")
} else if (currentDay == "THU") {
    console.log("Timings: 10:15 - 18:15")
} else if (currentDay == "TRI") {
    console.log("Timings: 09:05 - 17:05")
} else if (currentDay == "SAT") {
    console.log("Timings: 09:00 - 17:00")
} else if (currentDay == "SUN") {
    console.log("Timings: 09:00 - 13:00")
}

switch(currentDay) {
    case "MON": console.log("Timings: 10:00 - 18:00");
    break;
    case "TUE": console.log("Timings: 09:00 - 17:00");
    break;
    case "WED": console.log("Timings: 09:30 - 17:30");
    break;
    case "THU": console.log("Timings: 10:15 - 18:15");
    break;
    case "FRI": console.log("Timings: 09:05 - 17:05");
    break;
    case "SAT": console.log("Timings: 09:00 - 17:00");
    break;
    case "SUN": console.log("Timings: 09:00 - 13:00");
    break;
    default: console.log("Input is not a day.")
}


// 1. Program to print the number of days of a month using switch-case

var monthNumber = 8;

switch(monthNumber) {
    case 1: console.log("Month = January\nTotal number of days = 31");
            break;
    case 2: console.log("Month = February\nTotal number of days = 28");
            break;
    case 3: console.log("Month = March\nTotal number of days = 31");
            break;
    case 4: console.log("Month = April\nTotal number of days = 30");
            break;
    case 5: console.log("Month = May\nTotal number of days = 31");
            break;
    case 6: console.log("Month = Juny\nTotal number of days = 30");
            break;
    case 7: console.log("Month = July\nTotal number of days = 30");
            break;
    case 8: console.log("Month = August\nTotal number of days = 31");
            break;
    case 9: console.log("Month = September\nTotal number of days = 30");
            break;
    case 10: console.log("Month = October\nTotal number of days = 31");
            break;
    case 11: console.log("Month = November\nTotal number of days = 30");
            break;
    case 12: console.log("Month = December\nTotal number of days = 31");
            break;
}

// 2.Program to check vowel or consonant using switch-case

var enteredAlphabet = 'e'

switch(enteredAlphabet) {
    case 'a': console.log("'a' is vowel");
            break;
    case 'e': console.log("'e' is vowel");
            break;
    case 'i': console.log("'i' is vowel");
            break;
    case 'o': console.log("'o' is vowel");
            break;
    case 'u': console.log("'u' is vowel");
            break;
    default: console.log("'" + enteredAlphabet + "'" + " is consonant")
}

// 3. Program to check even or odd number using switch-case

var enteredNumber = 71;

switch(enteredNumber % 2) {
    case 0: console.log(enteredNumber + " is an even number");
        break;
    case 1: console.log(enteredNumber + " is an odd number");
        break;
}