// This is a comment in JS

console.clear(); // Clear Console

var firstNum = 10;
console.log(firstNum); // Console Output Function

var secondNum = 15;
console.log(secondNum); 

// Styles of variable names
// Camel Case (Recommended for JS)
var firstNum;
// Pascal Case
var FirstNum;
// Snake Case
var first_num;

// Rules for naming JS variables:
// - Name can contain letters, digits, _ and $
var mySubject1, math$;
// - Name cannot start with a digit
// var 1math;
// - Name can start with an _ or $
var _subjectMarks, $rankInClass;
// - Names are case sensitive
var x, X;
// - Names cannot be reserves keywords
// var var;