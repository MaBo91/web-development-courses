console.clear()

var num1 = 10 // Integer
var num2 = 10.9 // Decimal Number

console.log(typeof(num1))
console.log(typeof(num2)) // -> Type: number

// Addition
var num3 = 15
var sum = num3 + num2 + num1
console.log(sum)

// Subtraction
var diff = num3 - num1
console.log(diff)

// Multiplication
var mul = num1 * num3
console.log(mul)

// Division
var div = num3 / num1
console.log(div)


// Division by 0
var divByZero = num3 / 0 
console.log(divByZero)
console.log(typeof(divByZero)) // -> number

// Multiplication by String
var mulByString = num3 * 'A'
console.log(mulByString)
console.log(typeof(mulByString)) // -> NaN is stored as a number