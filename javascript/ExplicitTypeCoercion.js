var mNum = 15;
console.log(typeof(mNum));

// Explicit type conversion: to string with String()
var mStr = String(mNum); 
console.log(typeof(mStr));

console.log(typeof(String(null)));
console.log(typeof(String(undefined)));
console.log(typeof(String(-235.46)));
console.log(typeof(String(true))+"\n"); // -> Converts all to string data type

// Explicit type conversion: to number with Number()
console.log(typeof(Number('2')));
console.log(Number(true), typeof(Number(true)));
console.log(Number(false), typeof(Number(false)));
console.log(Number('-90.2134'), typeof(Number('-90.2134')));
console.log(Number('apple'), typeof(Number('apple')));
console.log("\n")

// Explicit type conversion: to boolean with Boolean()
console.log(Boolean(1), typeof(Boolean(1)));
console.log(Boolean(0), typeof(Boolean(0)));
console.log(Boolean('apple'), typeof(Boolean('apple')));
console.log(Boolean(null), typeof(Boolean(null)));
console.log(Boolean(undefined), typeof(Boolean(undefined)));
console.log(Boolean(""), typeof(Boolean("")));

// PRACTICE PROBLEMS
console.log("PRACTICE PROBLEMS");

// 1. What will be printed in the console?
console.log("\nQ1:");
console.log(String(123));
console.log(String(-12.3));
console.log(String(null));
console.log(String(undefined));
console.log(String(true));
console.log(String(false));

// 2. What will be printed in the console?
console.log("\nQ2:");
console.log(Boolean(""));
console.log(Boolean('Hello'));
console.log(Boolean(0));
console.log(Boolean(200));
console.log(Boolean(-0));
console.log(Boolean(-200));
console.log(Boolean(NaN));
console.log(Boolean(null));
console.log(Boolean(undefined));
console.log(Boolean(false));

// 3. What will be printed in the console?
console.log("\nQ3:");
console.log(Number(null));
console.log(Number(undefined));
console.log(Number(true));
console.log(Number(false));
console.log(Number(" 12 "));
console.log(Number("-12.34"));
console.log(Number("\n"));
console.log(Number(" 12s "));
console.log(Number(123));