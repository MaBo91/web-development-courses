var mVar;
console.log(mVar); // "undefined", because this variable has no value asigned: variable is declared, but not initialized

mVar = null;
console.log(mVar); // null is a value to represent that this variable holds no value

// PRACTICE PROBLEMS

var mNum;
console.log(mNum);

mNum = null;
console.log(mNum);

console.log(undefined == null);
console.log(undefined === null);