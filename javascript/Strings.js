// everything inside "" or '' is a string in JS
var myFirstString = "I love JavaScript";
console.log(typeof(myFirstString)); // -> string

var mySecondString = 'We will learn JavaScript together';
console.log(typeof(mySecondString)); // -> string

// What if my String contains a double quote
var doubleQuoteString = "This is a \"JavaScript\" string";
var doubleQuoteString2 = "This is a 'JavaScript' string";
console.log(doubleQuoteString, "\n", doubleQuoteString2);

// What if my String contains a single quote
var singleQuoteString = 'This is a \'JavaScript\' string';
var singleQuoteString2 = 'This is a "JavaScript" string';
console.log(singleQuoteString, "\n", singleQuoteString2);


