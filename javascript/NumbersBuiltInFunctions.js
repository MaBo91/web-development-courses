console.clear();


var num1 = 10;
var num2 = 20.3;

// toString()-Function: Takes number as input and returns a string
console.log(num1.toString());
console.log(num2.toString());


var strNum1 = "10";
var strNum2 = "11.5"
var strNum3 = "ABC"

// parseInt(): Takes string numeral as input and returns a number
console.log(parseInt(strNum1));
console.log(parseInt(strNum2)); // -> returns 11 
console.log(parseInt(strNum3))

// parseFloat(): Takes string numeral as input and returns a floating number
console.log(parseFloat(strNum2));
console.log(parseFloat(strNum3));

// toFixed(): Takes a floating number and rounds it off to given position
var strFloat = 87.824359032;
console.log(strFloat.toFixed()) // round to 88
console.log(strFloat.toFixed(1)) // round to 87.8
console.log(strFloat.toFixed(3)) // round to 87.824