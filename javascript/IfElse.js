// Keyword "if" and condition inside (...) to control, if the block { } is executed or not
{
    if (7 > 5) {
        console.log(true);
    } else { // else will be executed, when if condition returns false
        console.log(false);
    }
}

// If - else if - else to test multiple options, if all conditions are wrong, the else block will be executed
{
    if (5 > 7) {
        console.log("5 > 7");
    } else if (7 > 8) {
        console.log("7 > 8");
    } else {
        console.log("Nothing")
    }
}


// PRACTICE PROBLEMS

// 1. Check if the given lenght and breadth results in a square or not
{
    var lenght = 200;
    var breadth = 200;

    if(lenght == breadth){
        console.log("It's a square")
    }
    else {
        console.log("It's not a square")
    }
}

// 2. Find the largest out of three numbers
{
    var num1 = 5;
    var num2 = 8;
    var num3 = 20;

    if(num1 > num2) {
        if(num1 > num3) {
            console.log(num1 + " is the largest number of " + num1 + ", " + num2 + " and " + num3 + ".");
        }
        else {
            console.log(num3 + " is the largest number of " + num1 + ", " + num2 + " and " + num3 + ".");
        }
    }
    else {
        if(num2 > num3) {
            console.log(num2 + " is the largest number of " + num1 + ", " + num2 + " and " + num3 + ".");
        }
        else {
            console.log(num3 + " is the largest number of " + num1 + ", " + num2 + " and " + num3 + ".");
        }
    }
}

// 3. Grading System: Assign grades to reached points
{
    var enteredMarks = 60;

    if(enteredMarks <= 25) {
        console.log("F");
    } else if(enteredMarks >=25 && enteredMarks <=45) {
        console.log("E");
    } else if(enteredMarks >=45 && enteredMarks <=50) {
        console.log("D");
    } else if(enteredMarks >=50 && enteredMarks <=60) {
        console.log("C");
    } else if(enteredMarks >=60 && enteredMarks <=80) {
        console.log("B");
    } else {
        console.log("A");
    }
}

// 4. Calculate discount of a shop: 10% discount if the quantity is more than 1000, one unit costs 100
{
    var unitCost = 100;
    var numberOfItemsPurchased = 999;

    if(numberOfItemsPurchased >=1000) { // if more than 1000, get 10% discount
        console.log("10% Discount!\nTotal: " + numberOfItemsPurchased * unitCost * 0.9 + "€")
    } else {
        console.log("Total: " + unitCost * numberOfItemsPurchased + "€");
    }
}