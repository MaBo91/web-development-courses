var mFriends = ['Shaw', 'Clara', 'Vab', 'Kun', 'Jacob', 'Dina'];

console.log(mFriends, typeof(mFriends)); // arrays are a spectial type of object

console.log(mFriends[2]); // access data with position inside [], note: count from 0!
console.log(mFriends[0]); // returns 'Shaw'

// Update a value inside array
mFriends[0] = 'Shawn';
console.log(mFriends);

// Add more items to array
mFriends[6] = 'Alan'; // attach to the end
console.log(mFriends);
//mFriends[10] = 'Tiffany'; // adds 'Tiffany' to 10th position, position 7 - 9 will be undefined
//console.log(mFriends);
mFriends[mFriends.length] = "Rob"; // attach to last position of array 
console.log(mFriends);
mFriends.push('Jack'); // another way to add an item to the end of an array
console.log(mFriends);

// Delete a item from array
var mName = mFriends.pop(); // delete last item from array
console.log("Deleted person: " + mName);
var mName = mFriends.pop(); // delete last item from array
console.log("Deleted person: " + mName);
var mName = mFriends.pop(); // delete last item from array
console.log("Deleted person: " + mName);

// Insert items in array at specific position
console.log(mFriends);
mFriends.splice(3, 0, 'Donna', 'Rachel'); // Parameter: Start Position, Number of Items to remove, Items to be added
console.log(mFriends);
mFriends.splice(0, 0, 'Hardy'); // add item to first position without deleting any item
console.log(mFriends);

// Delete item in arry at specific position
mFriends.splice(2, 1); // delete 3rd item
console.log(mFriends);
mFriends.splice(0, 2); // remove first two items
console.log(mFriends);

// Concatenation (merge arrays together)
var mOfficeFriends = ['Nina', 'Aron', 'Kathie', 'Vic', 'Paula'];

var mAllFriends = mFriends.concat(mOfficeFriends); // merge array 'mFriends' with 'mOfficeFriends'
console.log(mFriends);
console.log(mOfficeFriends);    
console.log(mAllFriends);

// Sorting, Ascending and Descending an array
mAllFriends.sort(); // strings will be sorted in ascending order according to its first letter
console.log(mAllFriends);

mAllFriends.reverse(); // strings will be sorted in descending order according to its first letter
console.log(mAllFriends);


// PRACTICE PROBLEMS
console.log("\nPRACTICE PROBLEMS");

// 1. Write a JavaScript program to find the longest string from a given array

var stringArray = ['asdf', 'sd', 'something', 'right', 'position'];

var longestString = '';

for (let index = 0; index < stringArray.length; index++) {
    if (stringArray[index].length > longestString.length) {
        longestString = stringArray[index];
    } 
}

console.log("Longest string in array: " + longestString);


// 2. Write a JavaScript program to remove '0', false, undefined, null, NaN from given array

var numAndOthersArray = [NaN, 0, 15, false, -22, undefined, 47, null, 94];
var numArray = [];

for (let index = 0; index < numAndOthersArray.length; index++) {
    var currentItem = numAndOthersArray[index];
    if (!(Number.isNaN(currentItem) || currentItem == 0 || currentItem == false || currentItem == undefined || currentItem == null)) {
        numArray.push(numAndOthersArray[index]);
    }
}
console.log(numArray);


// 3. Write a JavaScript code to divide a given array of positive integers into two parts.
// First element goes to first part, second element goes to second part, and third element
// goes to first part and so on.
// Now compute the sum of two parts and store into an array of size two.

var numberArray = [1, 3, 6, 2, 5, 10];

var evenPositions = [], oddPositions = [], sum = [];

for(let index = 0; index < numberArray.length; index++) {
    if(index % 2 == 0) { // no remainders -> even position
        evenPositions.push(numberArray[index]);
    } else {
        oddPositions.push(numberArray[index]);
    }
}
console.log(evenPositions);
console.log(oddPositions);

sum.push(evenPositions[0] + evenPositions[1] + evenPositions[2]);
sum.push(oddPositions[0] + oddPositions[1] + oddPositions[2]);

console.log(sum);


// 4. Write a JavaScript program to check whether there is at least one element which occurs in two given sorted arrays of integers.

var arr1 = [1, 2, 3];
var arr2 = [3, 4, 5]; // should return true

var arr3 = [1, 2, 3];
var arr4 = [4, 5, 6]; // should return false

function compareItems(array1, array2) {
    var concatArray = array1.concat(array2);
    var lastItem;
    var occursTwice = false;

    concatArray.sort();

    for (let index = 0; index < concatArray.length; index++) {
        if(concatArray[index] == lastItem) {
            occursTwice = true
        }
        lastItem = concatArray[index];
    }

    return console.log(occursTwice);
}

compareItems(arr1, arr2);
compareItems(arr3, arr4);


// 5. Write a JavaScript function to find the difference of two arrays.

var myArray1 = [1, 2, 3];
var myArray2 = [100, 2, 1, 10];

function arrayDifference (array1, array2) {
    var a = [], diff = [];

    for(let index = 0; index < array1.length; index++) {
        a[array1[index]] = true; // set true at positions in array a[], that refer to the number in array1
    }
    
    for(let index = 0; index < array2.length; index++) {
        if(a[array2[index]]) { // if the position is already true, it means that this number occured already in array1, so delete it from a[]
            delete a[array2[index]];
        } else { // else set this position, that refers to the number an array2, to true
            a[array2[index]] = true;
        }
    }

    for (let index = 0; index < a.length; index++) { // now go through array a[], and save the positions where a 'true' occurs in a new array diff[]
        if(a[index]) {
            diff.push(index);
        }
    }

    return console.log(diff);
}

arrayDifference(myArray1, myArray2);


// 1. Write a JavaScript program to create a new array by reversing the elements of given array

var itemArray = [1,'a',2,'b',3,'c',6,'d',7,'e',8,'f'];

var reverseItemArray = itemArray.reverse();
console.log(reverseItemArray);


// 2. Write a JavaScript program to check if there is at least one element which occurs in two given sorted arrays of integers.

var numbArray1 = [4, 11, 12, 23, 46, 65, 70, 73, 98];
var numbArray2 = [7, 13, 25, 46, 58, 70, 84];

compareItems(numbArray1, numbArray2);


// 3. Write a JavaScript program to check whether a given array of integers represents either a strictly increasing or a
// strictly decreasing sequence.

var testArray = [4, 5, 6, 7, 8, 9];
var testArray2 = [3, 4, 6, 6, 7, 8, 9];

function isArraySequence(arr) {

    var isSequence = false;

    for(let index = 0; index < arr.length -1; index++) {
        if (arr[index] + 1 == arr[index+1]) {
            isSequence = true;
        } else {
            isSequence = false;
            break; // go out of loop if its false for the first time
        }
    }
    console.log(isSequence);
}

isArraySequence(testArray);


// 4. Write a JavaScript function to convert an amount to coins

var amount = 24;

function amountToCoins(amount) {

    var coinArray = [25, 10, 5, 2, 1];
    var initialAmount = amount;
    var coins = [];

    for(var i = 0; i < coinArray.length; i++) {
        if (amount >= coinArray[i]) {
            amount = amount - coinArray[i];
            coins.push(coinArray[i]);
            i--;
        }
    }

    console.log(initialAmount, coins);
}

amountToCoins(amount);


// 5. Write a JavaScript function to create a new array from given array by formatting the numbers to human readable form
// with correct suffix like 1 to 1st, 2 to 2nd, 3 to 3rd and 4 to 4th.

var inputArray = [22, 8, 301, 404, 35, 99, 43];

function ordinalSuffix(arr) {

    var j, k, currentItem;

    for(var i = 0; i < arr.length; i++) {
        currentItem = arr[i];

        j = currentItem % 10;
        k = currentItem % 100;

        if(j == 1 && k !== 11) {
            arr[i] += "st";
        }
        
        else if(j == 2 && k !== 12) {
            arr[i] += "nd";
        } 
        
        else if(j == 3 && k !== 13) {
            arr[i] += "rd"; 
        } 
        
        else {
            arr[i] += "th";
        }
    }

    return console.log(arr);
}

ordinalSuffix(inputArray);