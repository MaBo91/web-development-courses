var myFirstString = "This is a string for JavaScript string functions, JavaScript";

// Returns the length of a string
console.log(myFirstString.length);

// Find index of a string inside another string
console.log(myFirstString.indexOf("JavaScript"));
console.log(myFirstString.indexOf("This"));

// Find last index of a string inside another string
console.log(myFirstString.lastIndexOf("JavaScript"));
console.log(myFirstString.lastIndexOf("This"));

// Get a part of our string: slice(start, end)
console.log(myFirstString.slice(0, 4));
console.log(myFirstString.slice(21, 31));
console.log(myFirstString.slice(-10));
console.log(myFirstString.slice(5));

// Get sub string function: substr(startPos, length)
console.log(myFirstString.substr(0, 4));
console.log(myFirstString.substr(21, 10));
console.log(myFirstString.substr(21));

var exampleString = "This is JavaScript tutorial"

// toUpperCase() - Convert the string to uppercase characters
console.log(exampleString.toUpperCase());

// toLowerCase() - Convert the string to lowercase characters
console.log(exampleString.toLowerCase());

// concat - it merges two or more strings
var firstName = "JavaScript";
var lastName = "Playground";
console.log(exampleString.concat(firstName));
console.log(exampleString.concat(' ',firstName, ' ', lastName));

// We can also use '+' to concat two or more strings
console.log(firstName + ' ' + lastName + ' ' + exampleString);

// trim() - removes extra spaces
var extraSpaceString = "             mystring       ";
console.log(extraSpaceString.trim());

var extraSpaceString2 = "                my string    ";
console.log(extraSpaceString2.trim());

// charAt() - Takes a position as an arg and returns the character at this position
var charAtExampleString = "This is my test string";
console.log(charAtExampleString.charAt(5)); // -> returns 'i', count from 0
console.log(charAtExampleString.charAt(11));

// split() - It splits our string on the basis of the arguments passed
var sampleString = "This is my sample string";
console.log(sampleString.split(' ')); // -> Split strings, when ' ' occurs, and store them in an array

var sampleString2 = "This, is, my, sample, string";
console.log(sampleString2.split(','));

var sampleString3 = "This is awesome";
console.log(sampleString3.split());
