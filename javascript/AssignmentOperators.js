num1 = 1;
num2 = 30;

// assign values to variables with =
num1 = 15; 

// Shortcut for add and assign +=
num1 += 10; // num1 = num1 + 10;
console.log(num1);
num1 += num2; // num1 = num1 + num2;
console.log(num1);

// Shortcut for substract and assign -=
num1 -= 10;  // num1 = num1 - 10;
num1 -= num2; // num1 = num1 - num2;

// Shortcut for multiply and assign *=
num1 *= 10;  // num1 = num1 * 10;
num1 *= num2; // num1 = num1 * num2;

// Shortcut for divide and assign *=
num1 /= 10;  // num1 = num1 / 10;
num1 /= num2; // num1 = num1 / num2;

// Shortcut for modulus and assign %=
num1 %= 10;  // num1 = num1 % 10;
num1 %= num2; // num1 = num1 % num2;


// PRACITCE PROBLEMS
console.log("PRACTICE PROBLEMS");

// 1. With x = 0, which of the following statements will produce a value of 1?
var x = 0;
console.log(x++);
x = 0;
console.log(x = x + 1);
x = 0
console.log(x += 1);

// 2. With y = 1, which of the following statements will produce a value of 0?
var y = 1;
console.log(y--);
y = 1;
console.log(y = y - 1);
y = 1;
console.log(y -= 1);

// 3. What will be the result of the following statement?
var num = 100;
num += 5 * 10 / 2;
console.log(num);

// 4. What will be the result of the following statement?
var num = 100;
num *= 2 * 10 / 5;
console.log(num);

// 5. What will be the result of the following statement?
var num = 100;
num /= 2 * 8 - 15;
console.log(num);
