var mCars = {
    "p1" : "350 kph",
    "gallardo" : "320 kph",
    "veyron" : "409 kph",
    "agera" : "429 kph"
}

console.log(mCars, typeof(mCars));


var mAgera = {
    name: "Agera",
    manufacturer: {
        name: "Koenigsegg",
        location: "Sweden"
    },
    topSpeed: 429,
    color: "Black",
    spoilers: false,
    applyBreaks: function() {
        setTimeout(function() {
            console.log("Car Stopped!");
        }, 5000);
    }
}

console.log(mAgera.name);
console.log(mAgera.topSpeed);
console.log(mAgera.manufacturer);
console.log(mAgera.manufacturer.location);
console.log(mAgera.applyBreaks());

// PRACTICE PROBLEM

// 1. Give a premium membership to customers if the customer has made 5 orders of the minimum value of 20

console.log("\nPRACTICE PROBLEMS");

var customerData = {
    'Ben10': [22, 30, 11, 17, 15, 52, 27, 12],
    'Sameer': [5, 17, 30, 33, 40, 22, 26, 10, 11, 45],
    'Zeeshan': [22, 30, 11, 5, 17, 30, 6, 57] 
} 

for(var key in customerData) { // go through all keys
    var count = 0; // initialize count variable
    for (let index = 0; index < customerData[key].length; index++) { // go through each key value
        if(customerData[key][index] > 20) { // if value greater 20
            count++; // add to counter
        }
        
        if(count > 5) {
            console.log(key + " get's a Premium Membership\n");
        }
    }
}


// 2.  List properties of a JavaScript object: ['name', 'sclass', 'rollno']

Object: 
    student = {
        name: "David Rayy",
        sclass: "VI",
        rollno: 12
    };

console.log(Object.keys(student));


// 3. Delete 'rollno'-property and print the object before and after deleting this property

console.log(student);
console.log("Deleting 'rollno'-property from student object");
delete student.rollno;
console.log(student);


// 4. Get length of a JavaScript object

console.log("Length of JavaScript Object: " + Object.keys(student).length);


// 5. Write a JavaScript program to display the reading status of the following books

library = [
    {
        author: 'Bill Gates',
        title: 'The Road Ahead',
        readingStatus : true
    },

    {
        author: 'Steve Jobs',
        title: 'Walter Isaacson',
        readingStatus : true
    },

    {
        author: 'Suzuanne Collins',
        title: 'Mockingjay: The Final Book of The Hunger Games',
        readingStatus : false
    }
];

for (var book in library) { // go through library object
    if (library[book].readingStatus == true) {
        console.log("You have already read '" + library[book].title + "' by " + library[book].author);
    } else {
        console.log("You still need to read '" + library[book].title + "' by " + library[book].author);
    }
}